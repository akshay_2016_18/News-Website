from urllib2 import urlopen
import bs4 as bs
#u=urlopen("http://www.abc.net.au/news/entertainment/")
u=urlopen("http://www.abc.net.au/news/technology/")
#u=urlopen("http://www.abc.net.au/news/politics/")
r=u.read()
soup=bs.BeautifulSoup(r,'lxml')
image_list,final_list,headline,links,desc=[],[],[],[],[]
for div_tag in soup.find_all('div',attrs={"id":"collectionId-4"},):
    for article_tag in div_tag.find_all('article',limit=5):
        for head in article_tag.find_all('a',limit=1):
            headline.append(head['title'])
            x = "http://www.abc.net.au/" + str(head['href'])
            links.append(x)
        for summ in article_tag.find_all('p'):
            desc.append(summ.text)
        for imgimg in article_tag.find_all('img'):
            image_list.append(imgimg['src'])
print headline
