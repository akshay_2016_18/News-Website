# -*- coding: utf-8 -*-
from flask import Flask,render_template,url_for,redirect
from urllib2 import urlopen
from bs4 import BeautifulSoup
u=urlopen("https://www.nytimes.com/section/business/economy?src=busfn")
r=u.read()
soup=BeautifulSoup(r,'lxml')
image_list,final_list,headline,links,desc=[],[],[],[],[]
for ol_tag in soup.find_all('ol',attrs={"class":"story-menu theme-stream initial-set"},limit=1):
    for div_tag in ol_tag.find_all('div',attrs={"class":"story-meta"},limit=5):
        for head in div_tag.find_all('h2',attrs={"class":"headline"}):
            headline.append(head.text.encode('utf-8'))
            links.append(head.link) #noLinks Need to change this
        for summ in ol_tag.find_all('p',attrs={"class":"summary"}):
            desc.append(summ.text.encode('utf-8'))
        for imgimg in ol_tag.find_all('img'):
            image_list.append(imgimg['src'])
for i in range(len(headline)):
    headline[i]=headline[i].replace("\xe2\x80\x90","-")
    headline[i]=headline[i].replace("\xe2\x80\x91","-")
    headline[i]=headline[i].replace("\xe2\x80\x92","-")
    headline[i]=headline[i].replace("\xe2\x80\x93","-")
    headline[i]=headline[i].replace("\xe2\x80\x94","-")
    headline[i]=headline[i].replace("\xe2\x80\x95","-")
    headline[i]=headline[i].replace("\xe2\x80\x98","'")
    headline[i]=headline[i].replace("\xe2\x80\x99","'")
    headline[i]=headline[i].replace("\xe2\x80\x9d","</strong>")
    headline[i]=headline[i].replace("\xe2\x80\x9c","<strong>")

for i in range(len(desc)):
    desc[i]=desc[i].replace("\n","")
    desc[i]=desc[i].replace("\xe2\x80\x90","-")
    desc[i]=desc[i].replace("\xe2\x80\x91","-")
    desc[i]=desc[i].replace("\xe2\x80\x92","-")
    desc[i]=desc[i].replace("\xe2\x80\x93","-")
    desc[i]=desc[i].replace("\xe2\x80\x94","-")
    desc[i]=desc[i].replace("\xe2\x80\x95","-")
    desc[i]=desc[i].replace("\xe2\x80\x98","'")
    desc[i]=desc[i].replace("\xe2\x80\x99","'")
    desc[i]=desc[i].replace("\xe2\x80\x9d","</strong>")
    desc[i]=desc[i].replace("\xe2\x80\x9c","<strong>")
print image_list
print headline
print desc
