# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division, print_function, unicode_literals
from flask import Flask,render_template,url_for
from bs4 import BeautifulSoup,SoupStrainer
from urllib2 import urlopen



from sumy.parsers.html import HtmlParser
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.lsa import LsaSummarizer as Summarizer
from sumy.nlp.stemmers import Stemmer
from sumy.utils import get_stop_words
app=Flask(__name__)
#@app.route('/')
#def home():
u=urlopen("https://japantoday.com/")
r=u.read()
strainer=SoupStrainer('h3',attrs={"class":"font-smoothing text-base text-xstrong mt-15 space-bottom-xs"})
soup=BeautifulSoup(r,'lxml',parse_only=strainer)
lolk,headll,parall,mainnews=[],[],[],[]
for anc in soup.find_all('a',limit=3):
    x="https://japantoday.com"+anc['href']
    lolk.append(x)
    x=anc.text
    x=x.encode('utf-8')
    x=x.replace("\xe2\x80\x90","-")
    x=x.replace("\xe2\x80\x91","-")
    x=x.replace("\xe2\x80\x92","-")
    x=x.replace("\xe2\x80\x93","-")
    x=x.replace("\xe2\x80\x94","-")
    x=x.replace("\xe2\x80\x95","-")
    x=x.replace("\xe2\x80\x9c","<strong>")
    x=x.replace("\xe2\x80\x9d","</strong>")
    x=x.replace("\xe2\x80\x98","'")
    x=x.replace("\xe2\x80\x99","'")
    x=x.replace("\n","")
    x=x.replace("\t","")
    headll.append(x)
u=urlopen(lolk[0])
r=u.read()
strainer=SoupStrainer('div',attrs={"class":"text-large mb-40"})
soup=BeautifulSoup(r,'lxml',parse_only=strainer)
for pa in soup.find_all('p'):
    x=pa.text
    x=x.encode('utf-8')
    x=x.replace("\xe2\x80\x90","-")
    x=x.replace("\xe2\x80\x91","-")
    x=x.replace("\xe2\x80\x92","-")
    x=x.replace("\xe2\x80\x93","-")
    x=x.replace("\xe2\x80\x94","-")
    x=x.replace("\xe2\x80\x95","-")
    x=x.replace("\xe2\x80\x9c","")
    x=x.replace("\xe2\x80\x9d","")
    x=x.replace("\xe2\x80\x98","'")
    x=x.replace("\xe2\x80\x99","'")
    x=x.replace("\n","")
    x=x.replace("\t","")
    parall.append(x)
s="".join(parall)
f=open("plain_text.txt","w+")
f.write(s)
f.close()
LANGUAGE="english"
parser = PlaintextParser.from_file("plain_text.txt", Tokenizer(LANGUAGE))
stemmer = Stemmer(LANGUAGE)
summarizer = Summarizer(stemmer)
summarizer.stop_words = get_stop_words(LANGUAGE)

for sentence in summarizer(parser.document, 3):
    print(sentence)

#    return render_template('sample.html',desc=mainnews)
#if __name__=="__main__":
#    app.run(debug=True)
