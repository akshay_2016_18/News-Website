import requests
from bs4 import BeautifulSoup


url = "http://www.theaustralian.com.au/business/economics"
headers = {'User-Agent':'Mozilla/5.0'}
page = requests.get(url)
soup = BeautifulSoup(page.text, "html.parser")
image_list,final_list,headline,links,desc=[],[],[],[],[]
for div in soup.find_all('div',attrs={"class":"module-content"}):
    for h4_tag in div.find_all('h4',limit=5):
        headline.append(h4_tag.text)
        for a_tag in h4_tag.find_all('a'):
            links.append(a_tag['href'])
    for imgimg in div.find_all('img',attrs={"class":"thumbnail"},limit=1):
        print imgimg['src']
