# -*- coding: utf-8 -*-
from flask import Flask,render_template,url_for,redirect
from urllib2 import urlopen
from bs4 import BeautifulSoup,SoupStrainer
app = Flask(__name__)
main_des = []
@app.route('/')
def home():
    return render_template('Home.html')

"""@app.route('/developer')
def developer():
    return render_template('developer.html')
"""
@app.route('/top-3/india')
def india():
    u = urlopen("http://www.thehindu.com/tag/81/india/")
    r = u.read()
    soup=BeautifulSoup(r,'lxml')
    image_list,headline,desc,final_list = [],[],[],[]
    for a_tag in soup.find_all('div',attrs={"class":"story-card-33"}):
        w=a_tag.find('div',attrs={"class":"story-card-33-news"})
        lk=w.find('a')
        if lk['href']=="http://www.thehindu.com/opinion/interview/":
            continue
        else:
            for link in a_tag.find_all('a',attrs={"class":"story-card-33-img focuspoint"}):
                main_des.append(link['href'])
                if link.find('img'):
                    x=link.find('img')
                    image_list.append(x['data-src-template'])
                else:
                    image_list.append('/static/news.png')
                para=a_tag.find('p',attrs={"class":"story-card-33-heading"})
                y=para.find('a')
                y=str(y)
                if y.find("span")!=-1:
                    st=y.find("</span>")+8
                    en=y.find("</a>")
                    y=y[st:en]
                    headline.append(y)
                    t=a_tag.find('span',attrs={"class":"light-gray-color story-card-33-text hidden-xs"})
                    t=t.text.encode('utf-8')
                    desc.append(t)
                else:
                    y=para.find('a')
                    y=y.text
                    desc.append(y.encode('utf-8'))
    for s in image_list:
        s = s.replace("BINARY/thumbnail","alternates/FREE_300")
        final_list.append(s)
    for i in range(len(headline)):
        headline[i]=headline[i].replace("\xe2\x80\x90","-")
        headline[i]=headline[i].replace("\xe2\x80\x91","-")
        headline[i]=headline[i].replace("\xe2\x80\x92","-")
        headline[i]=headline[i].replace("\xe2\x80\x93","-")
        headline[i]=headline[i].replace("\xe2\x80\x94","-")
        headline[i]=headline[i].replace("\xe2\x80\x95","-")
        headline[i]=headline[i].replace("\xe2\x80\x98","'")
        headline[i]=headline[i].replace("\xe2\x80\x99","'")
        headline[i]=headline[i].replace("\xe2\x80\x9d","</strong>")
        headline[i]=headline[i].replace("\xe2\x80\x9c","<strong>")

    for i in range(len(desc)):
        desc[i]=desc[i].replace("\n","")
        desc[i]=desc[i].replace("\xe2\x80\x90","-")
        desc[i]=desc[i].replace("\xe2\x80\x91","-")
        desc[i]=desc[i].replace("\xe2\x80\x92","-")
        desc[i]=desc[i].replace("\xe2\x80\x93","-")
        desc[i]=desc[i].replace("\xe2\x80\x94","-")
        desc[i]=desc[i].replace("\xe2\x80\x95","-")
        desc[i]=desc[i].replace("\xe2\x80\x98","'")
        desc[i]=desc[i].replace("\xe2\x80\x99","'")
        desc[i]=desc[i].replace("\xe2\x80\x9d","</strong>")
        desc[i]=desc[i].replace("\xe2\x80\x9c","<strong>")
    return render_template("India.html",N=len(final_list),news=final_list,head=headline,des=desc,main_news=main_des)
@app.route('/Politics/india')
def politicsindia():
    url_list=["http://www.thehindu.com/tag/1422-1420-1349/political-development/","http://www.thehindu.com/tag/1420-1349/politics-general/","http://www.thehindu.com/tag/1349/politics/"]
    image_list,final_list,headline,desc,main_des,realnews=[],[],[],[],[],[]
    for ul in url_list:
        u=urlopen(ul)
        r=u.read()
        soup=BeautifulSoup(r,'lxml')
        for a_tag in soup.find_all('div',attrs={"class":"story-card-33"},limit=2):
            for link in a_tag.find_all('a',attrs={"class":"story-card-33-img focuspoint"},limit=1):
                if link.find('img'):
                    x=link.find('img')
                    image_list.append(x['data-src-template'])
                else:
                    image_list.append('/static/politics.png')
                main_des.append(link['href'])
            para=a_tag.find('p',attrs={"class":"story-card-33-heading"})
            y=para.find('a')
            y=str(y)
            if y.find("span")!=-1:
                st=y.find("</span>")+8
                en=y.find("</a>")
                y=y[st:en]
                headline.append(y)
                t=a_tag.find('span',attrs={"class":"light-gray-color story-card-33-text hidden-xs"})
                t=t.text.encode('utf-8')
                desc.append(t)
            else:
                y=para.find('a')
                y=y.text
                desc.append(y.encode('utf-8'))
    for s in image_list:
        s = s.replace("BINARY/thumbnail","alternates/FREE_300")
        final_list.append(s)
    for i in range(len(headline)):
        headline[i]=headline[i].replace("\xe2\x80\x90","-")
        headline[i]=headline[i].replace("\xe2\x80\x91","-")
        headline[i]=headline[i].replace("\xe2\x80\x92","-")
        headline[i]=headline[i].replace("\xe2\x80\x93","-")
        headline[i]=headline[i].replace("\xe2\x80\x94","-")
        headline[i]=headline[i].replace("\xe2\x80\x95","-")
        headline[i]=headline[i].replace("\xe2\x80\x98","'")
        headline[i]=headline[i].replace("\xe2\x80\x99","'")
        headline[i]=headline[i].replace("\xe2\x80\x9c","\"")
        headline[i]=headline[i].replace("\xe2\x80\x9d","</strong>")
        headline[i]=headline[i].replace("\xe2\x80\x9c","<strong>")

    for i in range(len(desc)):
        desc[i]=desc[i].replace("\n","")
        desc[i]=desc[i].replace("\xe2\x80\x90","-")
        desc[i]=desc[i].replace("\xe2\x80\x91","-")
        desc[i]=desc[i].replace("\xe2\x80\x92","-")
        desc[i]=desc[i].replace("\xe2\x80\x93","-")
        desc[i]=desc[i].replace("\xe2\x80\x94","-")
        desc[i]=desc[i].replace("\xe2\x80\x95","-")
        desc[i]=desc[i].replace("\xe2\x80\x98","'")
        desc[i]=desc[i].replace("\xe2\x80\x99","'")
        desc[i]=desc[i].replace("\xe2\x80\x9d","</strong>")
        desc[i]=desc[i].replace("\xe2\x80\x9c","<strong>")
    return render_template("India.html",N=len(final_list),news=final_list,head=headline,des=desc,main_news=main_des)
@app.route('/Sports/india')
def sportsindia():
    url_list=["http://www.thehindu.com/tag/1683-1569/cricket/","http://www.thehindu.com/tag/1802-1569/soccer/","http://www.thehindu.com/tag/1570-1569/hockey/","http://www.thehindu.com/tag/1892-1569/tennis/","http://www.thehindu.com/tag/1737-1569/marathon/","http://www.thehindu.com/tag/1689-1683-1569/test-cricket/"]
    image_list,final_list,headline,desc,main_des,realnews=[],[],[],[],[],[]
    for ul in url_list:
        u=urlopen(ul)
        r=u.read()
        soup=BeautifulSoup(r,'lxml')
        for a_tag in soup.find_all('div',attrs={"class":"story-card-33"},limit=1):
            for link in a_tag.find_all('a',attrs={"class":"story-card-33-img focuspoint"},limit=1):
                if link.find('img'):
                    x=link.find('img')
                    image_list.append(x['data-src-template'])
                else:
                    image_list.append('/static/sports.png')
                main_des.append(link['href'])
            para=a_tag.find('p',attrs={"class":"story-card-33-heading"})
            y=para.find('a')
            y=str(y)
            if y.find("span")!=-1:
                st=y.find("</span>")+8
                en=y.find("</a>")
                y=y[st:en]
                headline.append(y)
                t=a_tag.find('span',attrs={"class":"light-gray-color story-card-33-text hidden-xs"})
                t=t.text.encode('utf-8')
                desc.append(t)
            else:
                y=para.find('a')
                y=y.text
                desc.append(y.encode('utf-8'))
    for s in image_list:
        s = s.replace("BINARY/thumbnail","alternates/FREE_300")
        final_list.append(s)
    for i in range(len(headline)):
        headline[i]=headline[i].replace("\xe2\x80\x90","-")
        headline[i]=headline[i].replace("\xe2\x80\x91","-")
        headline[i]=headline[i].replace("\xe2\x80\x92","-")
        headline[i]=headline[i].replace("\xe2\x80\x93","-")
        headline[i]=headline[i].replace("\xe2\x80\x94","-")
        headline[i]=headline[i].replace("\xe2\x80\x95","-")
        headline[i]=headline[i].replace("\xe2\x80\x98","'")
        headline[i]=headline[i].replace("\xe2\x80\x99","'")
        headline[i]=headline[i].replace("\xe2\x80\x9c","\"")
        headline[i]=headline[i].replace("\xe2\x80\x9d","</strong>")
        headline[i]=headline[i].replace("\xe2\x80\x9c","<strong>")

    for i in range(len(desc)):
        desc[i]=desc[i].replace("\n","")
        desc[i]=desc[i].replace("\xe2\x80\x90","-")
        desc[i]=desc[i].replace("\xe2\x80\x91","-")
        desc[i]=desc[i].replace("\xe2\x80\x92","-")
        desc[i]=desc[i].replace("\xe2\x80\x93","-")
        desc[i]=desc[i].replace("\xe2\x80\x94","-")
        desc[i]=desc[i].replace("\xe2\x80\x95","-")
        desc[i]=desc[i].replace("\xe2\x80\x98","'")
        desc[i]=desc[i].replace("\xe2\x80\x99","'")
        desc[i]=desc[i].replace("\xe2\x80\x9d","</strong>")
        desc[i]=desc[i].replace("\xe2\x80\x9c","<strong>")
    return render_template("India.html",N=len(final_list),news=final_list,head=headline,des=desc,main_news=main_des)
@app.route('/economy/india')
def economyindia():
    u=urlopen("http://www.thehindu.com/tag/791-684/economy-general/")
    r=u.read()
    soup=BeautifulSoup(r,'lxml')
    image_list,headline,desc,final_list=[],[],[],[]
    for a_tag in soup.find_all('div',attrs={"class":"story-card-33"}):
        w=a_tag.find('div',attrs={"class":"story-card-33-news"})
        lk=w.find('a')
        if lk['href']=="http://www.thehindu.com/opinion/interview/":
            continue
        else:
            for link in a_tag.find_all('a',attrs={"class":"story-card-33-img focuspoint"}):
                main_des.append(link['href'])
                if link.find('img'):
                    x=link.find('img')
                    image_list.append(x['data-src-template'])
                else:
                    image_list.append('/static/news.png')
                para=a_tag.find('p',attrs={"class":"story-card-33-heading"})
                y=para.find('a')
                y=str(y)
                if y.find("span")!=-1:
                    st=y.find("</span>")+8
                    en=y.find("</a>")
                    y=y[st:en]
                    headline.append(y)
                    t=a_tag.find('span',attrs={"class":"light-gray-color story-card-33-text hidden-xs"})
                    t=t.text.encode('utf-8')
                    desc.append(t)
                else:
                    y=para.find('a')
                    y=y.text
                    desc.append(y.encode('utf-8'))
    for s in image_list:
        s = s.replace("BINARY/thumbnail","alternates/FREE_300")
        final_list.append(s)
    for i in range(len(headline)):
        headline[i]=headline[i].replace("\xe2\x80\x90","-")
        headline[i]=headline[i].replace("\xe2\x80\x91","-")
        headline[i]=headline[i].replace("\xe2\x80\x92","-")
        headline[i]=headline[i].replace("\xe2\x80\x93","-")
        headline[i]=headline[i].replace("\xe2\x80\x94","-")
        headline[i]=headline[i].replace("\xe2\x80\x95","-")
        headline[i]=headline[i].replace("\xe2\x80\x98","'")
        headline[i]=headline[i].replace("\xe2\x80\x99","'")
        headline[i]=headline[i].replace("\xe2\x80\x9c","\"")
        headline[i]=headline[i].replace("\xe2\x80\x9d","</strong>")
        headline[i]=headline[i].replace("\xe2\x80\x9c","<strong>")

    for i in range(len(desc)):
        desc[i]=desc[i].replace("\n","")
        desc[i]=desc[i].replace("\xe2\x80\x90","-")
        desc[i]=desc[i].replace("\xe2\x80\x91","-")
        desc[i]=desc[i].replace("\xe2\x80\x92","-")
        desc[i]=desc[i].replace("\xe2\x80\x93","-")
        desc[i]=desc[i].replace("\xe2\x80\x94","-")
        desc[i]=desc[i].replace("\xe2\x80\x95","-")
        desc[i]=desc[i].replace("\xe2\x80\x98","'")
        desc[i]=desc[i].replace("\xe2\x80\x99","'")
        desc[i]=desc[i].replace("\xe2\x80\x9d","</strong>")
        desc[i]=desc[i].replace("\xe2\x80\x9c","<strong>")
    return render_template("India.html",N=len(final_list),news=final_list,head=headline,des=desc,main_news=main_des)
@app.route('/Technology/india')
def technologyindia():
    url_list=["http://www.thehindu.com/tag/1506-1461/technology-general/","http://www.thehindu.com/tag/1461/science-and-technology/"]
    image_list,final_list,headline,desc,main_des,realnews=[],[],[],[],[],[]
    for ul in url_list:
        u=urlopen(ul)
        r=u.read()
        soup=BeautifulSoup(r,'lxml')
        for a_tag in soup.find_all('div',attrs={"class":"story-card-33"}):
            for link in a_tag.find_all('a',attrs={"class":"story-card-33-img focuspoint"}):
                if link.find('img'):
                    x=link.find('img')
                    image_list.append(x['data-src-template'])
                else:
                    image_list.append('/static/news.png')
                main_des.append(link['href'])
            para=a_tag.find('p',attrs={"class":"story-card-33-heading"})
            y=para.find('a')
            y=str(y)
            if y.find("span")!=-1:
                st=y.find("</span>")+8
                en=y.find("</a>")
                y=y[st:en]
                headline.append(y)
                t=a_tag.find('span',attrs={"class":"light-gray-color story-card-33-text hidden-xs"})
                t=t.text.encode('utf-8')
                desc.append(t)
            else:
                y=para.find('a')
                y=y.text
                desc.append(y.encode('utf-8'))
    for s in image_list:
        s = s.replace("BINARY/thumbnail","alternates/FREE_300")
        final_list.append(s)
    for i in range(len(headline)):
        headline[i]=headline[i].replace("\xe2\x80\x90","-")
        headline[i]=headline[i].replace("\xe2\x80\x91","-")
        headline[i]=headline[i].replace("\xe2\x80\x92","-")
        headline[i]=headline[i].replace("\xe2\x80\x93","-")
        headline[i]=headline[i].replace("\xe2\x80\x94","-")
        headline[i]=headline[i].replace("\xe2\x80\x95","-")
        headline[i]=headline[i].replace("\xe2\x80\x98","'")
        headline[i]=headline[i].replace("\xe2\x80\x99","'")
        headline[i]=headline[i].replace("\xe2\x80\x9c","\"")
        headline[i]=headline[i].replace("\xe2\x80\x9d","</strong>")
        headline[i]=headline[i].replace("\xe2\x80\x9c","<strong>")

    for i in range(len(desc)):
        desc[i]=desc[i].replace("\n","")
        desc[i]=desc[i].replace("\xe2\x80\x90","-")
        desc[i]=desc[i].replace("\xe2\x80\x91","-")
        desc[i]=desc[i].replace("\xe2\x80\x92","-")
        desc[i]=desc[i].replace("\xe2\x80\x93","-")
        desc[i]=desc[i].replace("\xe2\x80\x94","-")
        desc[i]=desc[i].replace("\xe2\x80\x95","-")
        desc[i]=desc[i].replace("\xe2\x80\x98","'")
        desc[i]=desc[i].replace("\xe2\x80\x99","'")
        desc[i]=desc[i].replace("\xe2\x80\x9d","</strong>")
        desc[i]=desc[i].replace("\xe2\x80\x9c","<strong>")
    return render_template("India.html",N=len(final_list),news=final_list,head=headline,des=desc,main_news=main_des)
@app.route('/Entertainment/india')
def entertainmentindia():
    url_list=["http://www.thehindu.com/tag/524-428/entertainment-general/","http://www.thehindu.com/tag/487-483-428/english-cinema/"]
    image_list,final_list,headline,desc,main_des,realnews=[],[],[],[],[],[]
    for ul in url_list:
        u=urlopen(ul)
        r=u.read()
        soup=BeautifulSoup(r,'lxml')
        for a_tag in soup.find_all('div',attrs={"class":"story-card-33"}):
            for link in a_tag.find_all('a',attrs={"class":"story-card-33-img focuspoint"}):
                if link.find('img'):
                    x=link.find('img')
                    image_list.append(x['data-src-template'])
                else:
                    image_list.append('/static/news.png')
                main_des.append(link['href'])
            para=a_tag.find('p',attrs={"class":"story-card-33-heading"})
            y=para.find('a')
            y=str(y)
            if y.find("span")!=-1:
                st=y.find("</span>")+8
                en=y.find("</a>")
                y=y[st:en]
                headline.append(y)
                t=a_tag.find('span',attrs={"class":"light-gray-color story-card-33-text hidden-xs"})
                t=t.text.encode('utf-8')
                desc.append(t)
            else:
                y=para.find('a')
                y=y.text
                desc.append(y.encode('utf-8'))
    for s in image_list:
        s = s.replace("BINARY/thumbnail","alternates/FREE_300")
        final_list.append(s)
    for i in range(len(headline)):
        headline[i]=headline[i].replace("\xe2\x80\x90","-")
        headline[i]=headline[i].replace("\xe2\x80\x91","-")
        headline[i]=headline[i].replace("\xe2\x80\x92","-")
        headline[i]=headline[i].replace("\xe2\x80\x93","-")
        headline[i]=headline[i].replace("\xe2\x80\x94","-")
        headline[i]=headline[i].replace("\xe2\x80\x95","-")
        headline[i]=headline[i].replace("\xe2\x80\x98","'")
        headline[i]=headline[i].replace("\xe2\x80\x99","'")
        headline[i]=headline[i].replace("\xe2\x80\x9d","</strong>")
        headline[i]=headline[i].replace("\xe2\x80\x9c","<strong>")

    for i in range(len(desc)):
        desc[i]=desc[i].replace("\n","")
        desc[i]=desc[i].replace("\xe2\x80\x90","-")
        desc[i]=desc[i].replace("\xe2\x80\x91","-")
        desc[i]=desc[i].replace("\xe2\x80\x92","-")
        desc[i]=desc[i].replace("\xe2\x80\x93","-")
        desc[i]=desc[i].replace("\xe2\x80\x94","-")
        desc[i]=desc[i].replace("\xe2\x80\x95","-")
        desc[i]=desc[i].replace("\xe2\x80\x98","'")
        desc[i]=desc[i].replace("\xe2\x80\x99","'")
        desc[i]=desc[i].replace("\xc3\xa8","e")
        desc[i]=desc[i].replace("\xe2\x80\x9d","</strong>")
        desc[i]=desc[i].replace("\xe2\x80\x9c","<strong>")
    return render_template("India.html",N=len(final_list),news=final_list,head=headline,des=desc,main_news=main_des)
@app.route('/top-3/USA')
def usa():
    u=urlopen("http://www.thehindu.com/tag/413-244/usa/")
    r=u.read()
    soup=BeautifulSoup(r,'lxml')
    image_list,final_list,headline,desc,main_des,realnews=[],[],[],[],[],[]
    for a_tag in soup.find_all('div',attrs={"class":"story-card-33"}):
        for link in a_tag.find_all('a',attrs={"class":"story-card-33-img focuspoint"}):
            if link.find('img'):
                x=link.find('img')
                image_list.append(x['data-proxy-image'])
            else:
                image_list.append('/static/news.png')
        para=a_tag.find('p',attrs={"class":"story-card-33-heading"})
        y=para.find('a')
        y=str(y)
        if y.find("span")!=-1:
            st=y.find("</span>")+8
            en=y.find("</a>")
            y=y[st:en]
            headline.append(y)
            t=a_tag.find('span',attrs={"class":"light-gray-color story-card-33-text hidden-xs"})
            t=t.text.encode('utf-8')
            desc.append(t)
        else:
            y=para.find('a')
            y=y.text
            desc.append(y.encode('utf-8'))
        main_des.append(link['href'])
    for s in image_list:
        s=s.replace("ALTERNATES","alternates")
        s=s.replace("100","300")
        final_list.append(s)
    for i in range(len(headline)):
        headline[i]=headline[i].replace("\xe2\x80\x90","-")
        headline[i]=headline[i].replace("\xe2\x80\x91","-")
        headline[i]=headline[i].replace("\xe2\x80\x92","-")
        headline[i]=headline[i].replace("\xe2\x80\x93","-")
        headline[i]=headline[i].replace("\xe2\x80\x94","-")
        headline[i]=headline[i].replace("\xe2\x80\x95","-")
        headline[i]=headline[i].replace("\xe2\x80\x98","'")
        headline[i]=headline[i].replace("\xe2\x80\x99","'")
        headline[i]=headline[i].replace("\xe2\x80\x9d","</strong>")
        headline[i]=headline[i].replace("\xe2\x80\x9c","<strong>")
    for i in range(len(desc)):
        desc[i]=desc[i].replace("\n","")
        desc[i]=desc[i].replace("\xe2\x80\x90","-")
        desc[i]=desc[i].replace("\xe2\x80\x91","-")
        desc[i]=desc[i].replace("\xe2\x80\x92","-")
        desc[i]=desc[i].replace("\xe2\x80\x93","-")
        desc[i]=desc[i].replace("\xe2\x80\x94","-")
        desc[i]=desc[i].replace("\xe2\x80\x95","-")
        desc[i]=desc[i].replace("\xe2\x80\x98","'")
        desc[i]=desc[i].replace("\xe2\x80\x99","'")
        desc[i]=desc[i].replace("\xc3\xa8","e")
        desc[i]=desc[i].replace("\xe2\x80\x9d","</strong>")
        desc[i]=desc[i].replace("\xe2\x80\x9c","<strong>")
    return render_template("India.html",news=final_list,head=headline,des=desc,main_news=main_des)

@app.route('/Politics/USA')
def usaPolitics():
    u=urlopen("https://www.nytimes.com/section/politics")
    r=u.read()
    soup=BeautifulSoup(r,'lxml')
    image_list,headline,links,desc=[],[],[],[]
    for ol_tag in soup.find_all('ol',attrs={"class":"story-menu"},limit=2):
        for li_tag in ol_tag.find_all('li',limit=2):
            for imgimg in li_tag.find_all('img',limit=1):
                if li_tag.find('img'):
                    image_list.append(imgimg['src'])
                else:
                    image_list.append('/static/news.png')
            for storybody in li_tag.find_all('div',attrs={"class":"story-body"}):
                for summ in storybody.find_all('p',attrs={"class":"summary"}):
                    x=summ.text
                    x=x.encode('utf-8')
                    desc.append(x)
                for head in storybody.find_all('h2',attrs={"class":"headline"}):
                    x=head.find('a')
                    links.append(x['href'])
                    y=x.text
                    y=y.encode('utf-8')
                    headline.append(y)
    for i in range(len(headline)):
        headline[i]=headline[i].replace("\n","")
        headline[i]=headline[i].replace("\xe2\x80\x99","'")
        headline[i]=headline[i].replace("\xe2\x80\x98","'")
        headline[i]=headline[i].replace("\xe2\x80\x9d","</strong>")
        headline[i]=headline[i].replace("\xe2\x80\x9c","<strong>")
        headline[i]=headline[i].replace("\xe2\x80\x94","-")
    for i in range(len(desc)):
        desc[i]=desc[i].replace("\n","")
        desc[i]=desc[i].replace("\xe2\x80\x98","'")
        desc[i]=desc[i].replace("\xe2\x80\x99","'")
        desc[i]=desc[i].replace("\xe2\x80\x9d","</strong>")
        desc[i]=desc[i].replace("\xe2\x80\x9c","<strong>")
        desc[i]=desc[i].replace("\xe2\x80\x94","-")
    return render_template("Politicsusa.html",news=image_list,head=headline,des=desc,main_news=links)
@app.route('/Sports/USA')
def usaSports():
    u=urlopen("https://www.nytimes.com/section/sports")
    r=u.read()
    soup=BeautifulSoup(r,'lxml')
    image_list,final_list,headline,links,desc=[],[],[],[],[]
    for ol_tag in soup.find_all('ol',attrs={"class":"story-menu"},limit=2):
        for li_tag in ol_tag.find_all('li',limit=2):
            for imgimg in li_tag.find_all('img',limit=1):
                if li_tag.find('img'):
                    image_list.append(imgimg['src'])
                else:
                    image_list.append('/static/news.png')
            for storybody in li_tag.find_all('div',attrs={"class":"story-body"}):
                for summ in storybody.find_all('p',attrs={"class":"summary"}):
                    x=summ.text
                    x=x.encode('utf-8')
                    desc.append(x)
                for head in storybody.find_all('h2',attrs={"class":"headline"}):
                    x=head.find('a')
                    links.append(x['href'])
                    y=x.text
                    y=y.encode('utf-8')
                    headline.append(y)
    for i in range(len(headline)):
        headline[i]=headline[i].replace("\n","")
        headline[i]=headline[i].replace("\xe2\x80\x99","'")
        headline[i]=headline[i].replace("\xe2\x80\x98","'")
        headline[i]=headline[i].replace("\xe2\x80\x9d","</strong>")
        headline[i]=headline[i].replace("\xe2\x80\x9c","<strong>")
        headline[i]=headline[i].replace("\xe2\x80\x94","-")
        headline[i]=headline[i].replace("\xc3\xb1","n")
    for i in range(len(desc)):
        desc[i]=desc[i].replace("\n","")
        desc[i]=desc[i].replace("\xc3\xad","i")
        desc[i]=desc[i].replace("\xe2\x80\x98","'")
        desc[i]=desc[i].replace("\xe2\x80\x99","'")
        desc[i]=desc[i].replace("\xe2\x80\x9d","</strong>")
        desc[i]=desc[i].replace("\xe2\x80\x9c","<strong>")
        desc[i]=desc[i].replace("\xe2\x80\x94","-")
    return render_template("Politicsusa.html",news=image_list,head=headline,des=desc,main_news=links)

@app.route('/technology/USA')
def usaTech():
    u=urlopen("https://www.nytimes.com/section/technology")
    r=u.read()
    soup=BeautifulSoup(r,'lxml')
    image_list,final_list,headline,links,desc=[],[],[],[],[]
    for ol_tag in soup.find_all('ol',attrs={"class":"story-menu"},limit=2):
        for li_tag in ol_tag.find_all('li',limit=2):
            for imgimg in li_tag.find_all('img',limit=1):
                if li_tag.find('img'):
                    image_list.append(imgimg['src'])
                else:
                    image_list.append('/static/news.png')
            for storybody in li_tag.find_all('div',attrs={"class":"story-body"}):
                for summ in storybody.find_all('p',attrs={"class":"summary"}):
                    x=summ.text
                    x=x.encode('utf-8')
                    desc.append(x)
                for head in storybody.find_all('h2',attrs={"class":"headline"}):
                    x=head.find('a')
                    links.append(x['href'])
                    y=x.text
                    y=y.encode('utf-8')
                    headline.append(y)
    for i in range(len(headline)):
        headline[i]=headline[i].replace("\n","")
        headline[i]=headline[i].replace("\xe2\x80\x99","'")
        headline[i]=headline[i].replace("\xe2\x80\x98","'")
        headline[i]=headline[i].replace("\xe2\x80\x9d","</strong>")
        headline[i]=headline[i].replace("\xe2\x80\x9c","<strong>")
        headline[i]=headline[i].replace("\xe2\x80\x94","-")
        headline[i]=headline[i].replace("\xc3\xb1","n")
    for i in range(len(desc)):
        desc[i]=desc[i].replace("\n","")
        desc[i]=desc[i].replace("\xc3\xad","i")
        desc[i]=desc[i].replace("\xe2\x80\x98","'")
        desc[i]=desc[i].replace("\xe2\x80\x99","'")
        desc[i]=desc[i].replace("\xe2\x80\x9d","</strong>")
        desc[i]=desc[i].replace("\xe2\x80\x9c","<strong>")
        desc[i]=desc[i].replace("\xe2\x80\x94","-")
    return render_template("Politicsusa.html",news=image_list,head=headline,des=desc,main_news=links)

@app.route('/Economy/USA')
def usaEconomy():
    u=urlopen("https://www.nytimes.com/section/business/economy?src=busfn")
    r=u.read()
    soup=BeautifulSoup(r,'lxml')
    image_list,final_list,headline,links,desc=[],[],[],[],[]
    for ol_tag in soup.find_all('ol',attrs={"class":"story-menu theme-stream initial-set"},limit=1):
        for div_tag in ol_tag.find_all('div',attrs={"class":"story-meta"},limit=5):
            for head in div_tag.find_all('h2',attrs={"class":"headline"}):
                headline.append(head.text)
                links.append(head.link) #noLinks Need to change this
            for summ in ol_tag.find_all('p',attrs={"class":"summary"}):
                desc.append(summ.text)
            for imgimg in ol_tag.find_all('img'):
                image_list.append(imgimg['src'])
    for i in range(len(headline)):
        headline[i]=headline[i].replace("\n","")
        headline[i]=headline[i].replace("\xe2\x80\x99","'")
        headline[i]=headline[i].replace("\xe2\x80\x9d","</strong>")
        headline[i]=headline[i].replace("\xe2\x80\x9c","<strong>")
        headline[i]=headline[i].replace("\xe2\x80\x94","-")
    for i in range(len(desc)):
        desc[i]=desc[i].replace("\n","")
        desc[i]=desc[i].replace("\xe2\x80\x99","'")
        desc[i]=desc[i].replace("\xe2\x80\x9d","</strong>")
        desc[i]=desc[i].replace("\xe2\x80\x9c","<strong>")
        desc[i]=desc[i].replace("\xe2\x80\x94","-")
    return render_template("Economyusa.html",news=image_list,head=headline,des=desc,main_news=links)
if __name__=='__main__':
    app.run(debug=True)
